module.exports = {
    context: __dirname,
    entry: {
        app: __dirname + "/src/app"
    },

    output: {
        path: __dirname + "/public-ts",
        filename: "[name].js"
    },

    module: {
        rules: [
            {
                test: /\.ts?$/,
                loader: "ts-loader",
                exclude: /node_modules/
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                 {
                  loader: "style-loader",
                  options: {
                   hmr: false,
                  },
                 },
                 {
                  loader: "css-loader",
                  options: {
                   url: false,
                   importLoaders: 1,
                  },
                 },
                 "postcss-loader",
                 "sass-loader",
                ],
            },
        ]
    },

    resolve: {
        extensions: [".ts"]
    },

    plugins: []
};