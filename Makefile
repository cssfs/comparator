.PHONY: up app down view

up:
	sudo docker-compose up -d

app:
	sudo docker exec -it css_framework_comparator bash

down:
	sudo docker-compose down

view:
	google-chrome public/index.html