let filterOpen = false;
let initleft = -20;
let intervalIdentity = null;

const filterToggle = function() {
    const $mobFilter = document.getElementById("item1");

    window.clearInterval(intervalIdentity);
    if (filterOpen) {
        let left = initleft;
        filterOpen = false;
        intervalIdentity = setInterval(function() {
            left--;
            $mobFilter.style.left = left + "em";
            if (left <= 0) {
                window.clearInterval(intervalIdentity);
            }
        }, 0);
    } else {
        let left = 0;
        filterOpen = true;
        intervalIdentity = setInterval(function() {
            left++;
            $mobFilter.style.left = left + "em";
            if (left >= initleft) {
                window.clearInterval(intervalIdentity);
            }
        }, 0);
    }
};