(function() {
    frameworkDataProvider("./assets/js/frameworks.json")
        .then(frameworks => {
            const $lessSizeZip = document.getElementById("js-checkbox-sort-by-size-zip");

            const $footer = document.getElementById("footer-text");
            $footer.innerHTML = `&copy ${new Date().getFullYear()}. Made with ` + '<span>❤</span> ' + 'by ' + '<a href="https://devspace.com.ua/" target="_blank">DevSpace Team</a>';

            function sortBySizeZip(a, b) {
                // ASC
                return a.size.gzipped - b.size.gzipped;
            }

            $lessSizeZip.addEventListener("click", function() {
                // sort O(N *ln(N)), so better match than sort
                render(frameworks.sort(sortBySizeZip));
            });

            render(frameworks);

            // why replace loop string concationation to join()
            // https://www.sitepoint.com/javascript-fast-string-concatenation/
            function render(list) {
                const views = new Array(list.length);

                for (let i = 0; i < list.length; i++) {
                    const item = list[i];

                    views[i] = `<tr>
                    <td>
                        ${item.name}
                    </td>
                    <td>
                        ${item.size.uncompressed}
                    </td>
                    <td>
                        ${item.size.minified}
                    </td>
                    <td>
                        ${item.size.gzipped}
                    </td>
                    <td>
                        <a href="${item.siteUrl}">${item.siteUrl}</a>
                    </td>
                </tr>`;
                }

                document.getElementById("demo").innerHTML = views.join("");
            }
        })
})();