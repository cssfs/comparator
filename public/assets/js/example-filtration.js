const FILTER_ACCESSIBILITY = "accessibility";

function matchAll(framework) {
    return true;
}

function matchAccessibility(framework) {
    return framework.accessibility;
}

const matchStateMap = {
    [FILTER_ACCESSIBILITY]: matchAll,
};

function match(frameworks) {
    const result = frameworks
        .filter(matchStateMap[FILTER_ACCESSIBILITY]);
        // other filtration

    return result;
}

function $(id) {
    return document.getElementById(id);
}

function render(list) {
    const views = new Array(list.length);

    for (let i = 0; i < list.length; i++) {
        const item = list[i];

        views[i] = `<h2><a href="${item.siteUrl}" target="_blank">${item.name} v${item.version}</a></h2>`;
    }

    $("demo").innerHTML = views.join("");
}

function onChecked($element, name, filter) {
    $element.addEventListener("click", function () {
        if ($element.checked) {
            matchStateMap[name] = filter;
        } else {
            matchStateMap[name] = matchAll;
        }

        render(match(frameworks));
    });
}

const $accessibilityCheckbox = $("js-checkbox-accessibility");
onChecked($accessibilityCheckbox, FILTER_ACCESSIBILITY, matchAccessibility);