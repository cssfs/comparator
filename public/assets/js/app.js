// The first step on the journey to beautiful, modular JavaScript is to learn the art of the self-executing anonymous function
(function () {
    
    frameworkDataProvider("./assets/js/frameworks.json")
        .then(frameworks => {
            function $(id) {
                return document.getElementById(id);
            }
        
            const FILTER_ACCESSIBILITY = "accessibility";
            const FILTER_STARS = "stars";
            const FILTER_DEPENDENCY_FREE = "dependency-free";
            const FILTER_GITTER = "gitter";
            const FILTER_FORUM = "forum";
            const FILTER_QUESTIONS = "questions";
            const FILTER_UNCOMPRESSED = "uncompressed";
            const FILTER_MINIFIED = "minified";
            const FILTER_GZIPPED = "gzipped";
            const FILTER_ADAPTABILITY_DESKTOP = "adaptability-desktop";
            const FILTER_ADAPTABILITY_TABLET = "adaptability-tablet";
            const FILTER_ADAPTABILITY_MOBILE = "adaptability-mobile";
        
            function matchAll(framework) {
                return true;
            }
        
            function matchAccessibility(framework) {
                return framework.accessibility;
            }
        
            function matchDependency(framework) {
                return framework.dependencyfree;
            }
        
            function matchAdaptabilityDesktop(framework) {
                return framework.adaptability.desktop;
            }
        
            function matchAdaptabilityTablet(framework) {
                return framework.adaptability.tablet;
            }
        
            function matchAdaptabilityMobile(framework) {
                return framework.adaptability.mobile;
            }
        
            function matchGitter(framework) {
                return framework.gitter !== null;
            }
        
            function matchForum(framework) {
                return framework.forum !== null;
            }
        
            function buildMatchStars(from, to) {
                if (to === 0) {
                    return function (framework) {
                        return framework.repository.stars >= from;
                    }
                }
        
                return function (framework) {
                    return framework.repository.stars >= from && framework.repository.stars <= to;
                }
            }
        
            function buildMatchQuestions(from, to) {
                if (to === 0) {
                    return function (framework) {
                        return framework.stackoverflow.questions >= from;
                    }
                }
        
                return function (framework) {
                    return framework.stackoverflow.questions >= from && framework.stackoverflow.questions <= to;
                }
            }
        
            function buildMatchUncompressed(from, to) {
                if (to === 0) {
                    return function (framework) {
                        return framework.size.uncompressed >= from;
                    }
                }
        
                return function (framework) {
                    return framework.size.uncompressed >= from && framework.size.uncompressed <= to;
                }
            }
        
            function buildMatchMinified(from, to) {
                if (to === 0) {
                    return function (framework) {
                        return framework.size.minified >= from;
                    }
                }
        
                return function (framework) {
                    return framework.size.minified >= from && framework.size.minified <= to;
                }
            }
        
            function buildMatchGzipped(from, to) {
                if (to === 0) {
                    return function (framework) {
                        return framework.size.gzipped >= from;
                    }
                }
        
                return function (framework) {
                    return framework.size.gzipped >= from && framework.size.gzipped <= to;
                }
            }
        
            let compare = null;
            const matchStateMap = {
                [FILTER_ACCESSIBILITY]: matchAll,
                [FILTER_STARS]: matchAll,
                [FILTER_DEPENDENCY_FREE]: matchAll,
                [FILTER_QUESTIONS]: matchAll,
                [FILTER_UNCOMPRESSED]: matchAll,
                [FILTER_MINIFIED]: matchAll,
                [FILTER_GZIPPED]: matchAll,
                [FILTER_GITTER]: matchAll,
                [FILTER_FORUM]: matchAll,
                [FILTER_ADAPTABILITY_DESKTOP]: matchAll,
                [FILTER_ADAPTABILITY_TABLET]: matchAll,
                [FILTER_ADAPTABILITY_MOBILE]: matchAll,
            };
        
            function match(frameworks) {
                const result = frameworks
                    .filter(matchStateMap[FILTER_GITTER])
                    .filter(matchStateMap[FILTER_FORUM])
                    .filter(matchStateMap[FILTER_ACCESSIBILITY])
                    .filter(matchStateMap[FILTER_STARS])
                    .filter(matchStateMap[FILTER_DEPENDENCY_FREE])
                    .filter(matchStateMap[FILTER_QUESTIONS])
                    .filter(matchStateMap[FILTER_UNCOMPRESSED])
                    .filter(matchStateMap[FILTER_MINIFIED])
                    .filter(matchStateMap[FILTER_GZIPPED])
                    .filter(matchStateMap[FILTER_ADAPTABILITY_DESKTOP])
                    .filter(matchStateMap[FILTER_ADAPTABILITY_TABLET])
                    .filter(matchStateMap[FILTER_ADAPTABILITY_MOBILE])
        
                // sort O(N *ln(N)), so better first match, than sort
                if (compare !== null) {
                    result.sort(compare);
                }
        
                return result;
            }
        
            const $fromStars = $("js-checkbox-star-from");
            const $toStars = $("js-checkbox-star-to");
            const $accessibilityCheckbox = $("js-checkbox-accessibility");
            const $dependencyCheckbox = $("js-checkbox-dependency-free");
            const $adaptabilityDesktopCheckbox = $("js-checkbox-adaptability-desktop");
            const $adaptabilityTabletCheckbox = $("js-checkbox-adaptability-tablet");
            const $adaptabilityMobileCheckbox = $("js-checkbox-adaptability-mobile");
            const $gitterCheckbox = $("js-checkbox-gitter");
            const $forumCheckbox = $("js-checkbox-forum");
            const $questionsFrom = $("js-checkbox-stack-from");
            const $questionsTo = $("js-checkbox-stack-to");
            const $moreStars = $("js-checkbox-sort-by-starts");
            const $moreQuestions = $("js-checkbox-sort-by-questions");
            const $lessSizeZip = $("js-checkbox-sort-by-size-zip");
            const $uncompressedFrom = $("js-checkbox-uncompressed-from");
            const $uncompressedTo = $("js-checkbox-uncompressed-to");
            const $minifiedFrom = $("js-checkbox-min-from");
            const $minifiedTo = $("js-checkbox-min-to");
            const $gzippedFrom = $("js-checkbox-gz-from");
            const $gzippedTo = $("js-checkbox-gz-to");
            
            const $footer = $("footer-text");
            $footer.innerHTML = `&copy ${new Date().getFullYear()}. Made with ` + '<span>❤</span> ' + 'by ' + '<a href="https://devspace.com.ua/" target="_blank">DevSpace Team</a>';
        
            function sortByQuestionsfunction(a, b) {
                // DESC
                return b.stackoverflow.questions - a.stackoverflow.questions;
            }
        
            function sortByStars(a, b) {
                // DESC
                return b.repository.stars - a.repository.stars;
            }
        
            function sortBySizeZip(a, b) {
                // ASC
                return a.size.gzipped - b.size.gzipped;
            }
        
            function onSort($element, sort) {
                $element.addEventListener("click", function () {
                    compare = sort;
        
                    render(match(frameworks));
                });
            }
        
            onSort($moreStars, sortByStars);
            onSort($moreQuestions, sortByQuestionsfunction);
            onSort($lessSizeZip, sortBySizeZip);
        
            function onChecked($element, name, filter) {
                $element.addEventListener("click", function () {
                    if ($element.checked) {
                        matchStateMap[name] = filter;
                    } else {
                        matchStateMap[name] = matchAll;
                    }
        
                    render(match(frameworks));
                });
            }
        
            onChecked($accessibilityCheckbox, FILTER_ACCESSIBILITY, matchAccessibility);
            onChecked($dependencyCheckbox, FILTER_DEPENDENCY_FREE, matchDependency);
            onChecked($gitterCheckbox, FILTER_GITTER, matchGitter);
            onChecked($forumCheckbox, FILTER_FORUM, matchForum);
            onChecked($adaptabilityDesktopCheckbox, FILTER_ADAPTABILITY_DESKTOP, matchAdaptabilityDesktop);
            onChecked($adaptabilityTabletCheckbox, FILTER_ADAPTABILITY_TABLET, matchAdaptabilityTablet);
            onChecked($adaptabilityMobileCheckbox, FILTER_ADAPTABILITY_MOBILE, matchAdaptabilityMobile);
        
            function buildMatchStarsByState() {
                const from = $fromStars.value ? parseInt($fromStars.value) : 0;
                const to = $toStars.value ? parseInt($toStars.value) : 0;
        
                matchStateMap[FILTER_STARS] = buildMatchStars(
                    from,
                    to
                );
            }
        
            function buildMatchQuestionsByState() {
                const from = $questionsFrom.value ? parseInt($questionsFrom.value) : 0;
                const to = $questionsTo.value ? parseInt($questionsTo.value) : 0;
        
                matchStateMap[FILTER_QUESTIONS] = buildMatchQuestions(
                    from,
                    to
                );
            }
        
            function buildMatchUncompressedByState() {
                const from = $uncompressedFrom.value ? parseInt($uncompressedFrom.value) : 0;
                const to = $uncompressedTo.value ? parseInt($uncompressedTo.value) : 0;
        
                matchStateMap[FILTER_UNCOMPRESSED] = buildMatchUncompressed(
                    from,
                    to
                );
            }
        
            function buildMatchMinifiedByState() {
                const from = $minifiedFrom.value ? parseInt($minifiedFrom.value) : 0;
                const to = $minifiedTo.value ? parseInt($minifiedTo.value) : 0;
        
                matchStateMap[FILTER_MINIFIED] = buildMatchMinified(
                    from,
                    to
                );
            }
        
            function buildMatchGzippedByState() {
                const from = $gzippedFrom.value ? parseInt($gzippedFrom.value) : 0;
                const to = $gzippedTo.value ? parseInt($gzippedTo.value) : 0;
        
                matchStateMap[FILTER_GZIPPED] = buildMatchGzipped(
                    from,
                    to
                );
            }
        
            $fromStars.addEventListener("keyup", function () {
                buildMatchStarsByState();
        
                render(match(frameworks));
            });
        
            $toStars.addEventListener("keyup", function () {
                buildMatchStarsByState();
        
                render(match(frameworks));
            });
        
            $questionsFrom.addEventListener("keyup", function () {
                buildMatchQuestionsByState();
        
                render(match(frameworks));
            });
        
            $questionsTo.addEventListener("keyup", function () {
                buildMatchQuestionsByState();
        
                render(match(frameworks));
            });
        
            $uncompressedFrom.addEventListener("keyup", function () {
                buildMatchUncompressedByState();
        
                render(match(frameworks));
            });
        
            $uncompressedTo.addEventListener("keyup", function () {
                buildMatchUncompressedByState();
        
                render(match(frameworks));
            });
        
            $minifiedFrom.addEventListener("keyup", function () {
                buildMatchMinifiedByState();
        
                render(match(frameworks));
            });
        
            $minifiedTo.addEventListener("keyup", function () {
                buildMatchMinifiedByState();
        
                render(match(frameworks));
            });
        
            $gzippedFrom.addEventListener("keyup", function () {
                buildMatchGzippedByState();
        
                render(match(frameworks));
            });
        
            $gzippedTo.addEventListener("keyup", function () {
                buildMatchGzippedByState();
        
                render(match(frameworks));
            });
        
            render(frameworks);
        
            // why replace loop string concationation to join()
            // https://www.sitepoint.com/javascript-fast-string-concatenation/
            function render(list) {
                const views = new Array(list.length);
        
                for (let i = 0; i < list.length; i++) {
                    const item = list[i];
        
                    views[i] = `<div class="card">
                        <div class="card-top">
                            <h2><a href="${item.siteUrl}" target="_blank">${item.name} v${item.version}</a></h2>
                            <h2><a href="${item.corporation.url}" target="_blank">${item.corporation.name}</a></h2>
                            <h2><a href="${item.repository.url}" target="_blank">⭐ ${item.repository.stars}</a></h2>
                        </div>
                        <div class="card-footer">
                            <h3>Accesibility: ${item.accessibility ? "✅" : "❌"}</h3>
                            <h3>Dependency: ${item.dependencyfree ? "❌" : `${item.requires}`}</h3>
                            <h3>${item.sass ? '<img src="./assets/images/sass.png" style="height:40px">' : ''}</h3>
                            <h3>${item.less ? '<img src="./assets/images/less.png" style="height:40px">' : ''}</h3>
                            <h3>License: ${item.license}</h3>
                        </div>
                        <div class="card-footer">
                            <h3>${item.adaptability.desktop ? '<img src="./assets/images/desktoptrue.png" style="height:80px">' : '<img src="./assets/images/desktopfalse.png" style="height:80px">'}</h3>
                            <h3>${item.adaptability.tablet ? '<img src="./assets/images/tablettrue.png" style="height:80px">' : '<img src="./assets/images/tabletfalse.png" style="height:80px">'}</h3>
                            <h3>${item.adaptability.mobile ? '<img src="./assets/images/mobiletrue.png" style="height:80px">' : '<img src="./assets/images/mobilefalse.png" style="height:80px">'}</h3>
                        </div> 
                        <div class="card-footer">
                            <h3>Uncompressed: ${item.size.uncompressed}</h3>
                            <h3>Minified: ${item.size.minified}</h3>
                            <h3>Gzipped: ${item.size.gzipped}</h3>
                        </div> 
                        <div class="social">
                            <h3><a href="${item.stackoverflow.tag}" target="_blank"><img class="stack-logo" src="./assets/images/logo_stackoverflow.png" />${item.stackoverflow.questions}</a></h3> 
                            ${gitter(item)} 
                            ${forum(item)} 
                        </div>  
                    </div>`;
                }
        
                $("demo").innerHTML = views.join("");
                $("founded").innerHTML = `Found ${list.length} of ${frameworks.length} frameworks`
            }
        
            function gitter(framework) {
                if (framework.gitter === null) {
                    return "";
                }
        
                return `<h3><a href="${framework.gitter.url}" target="_blank"><img class="stack-logo" src="./assets/images/gitter.ico" />${framework.gitter.members} members</a></h3>`;
            }
        
            function forum(framework) {
                if (framework.forum === null) {
                    return "";
                }
        
                return `<h3><a href="${framework.forum.url}" target="_blank">Forum ${framework.forum.members} members ${framework.forum.topics} topics</a></h3>`;
            }
        
            let expanded = false;
            $("select").addEventListener("click", function() {
                const checkboxes = $("checkboxes");
                if (!expanded) {
                    checkboxes.style.display = "block";
                    expanded = true;
                  } else {
                    checkboxes.style.display = "none";
                    expanded = false;
                  }
            });
        })

})();

// require to read https://learn.javascript.ru/coding-style