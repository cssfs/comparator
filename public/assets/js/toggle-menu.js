// add global variables...
let slideOpen = false;
let initHeight = 265;
let intervalId = null;

const slideToggle = function() {
    const $mobile = document.getElementById("mobile");

    window.clearInterval(intervalId);
    if (slideOpen) {
        let height = initHeight;
        slideOpen = false;
        intervalId = setInterval(function() {
            height--;
            $mobile.style.height = height + "px";
            if (height <= 0) {
                window.clearInterval(intervalId);
            }
        }, 0);
    } else {
        let height = 0;
        slideOpen = true;
        intervalId = setInterval(function() {
            height++;
            $mobile.style.height = height + "px";
            if (height >= initHeight) {
                window.clearInterval(intervalId);
            }
        }, 0);
    }
}