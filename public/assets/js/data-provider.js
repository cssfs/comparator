// inline data on development

function frameworkDataProvider(url) {
  return fetch(url)
    .then(res => res.json())
    .then(json => json)
}