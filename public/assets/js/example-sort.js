let compare = null;
const matchStateMap = {
    [FILTER_ACCESSIBILITY]: matchAll,
    // ...
};

function match(frameworks) {
    const result = frameworks
        .filter(matchStateMap[FILTER_ACCESSIBILITY]);
        // other filtration

    // sort O(N *ln(N)), so better first match, than sort
    if (compare !== null) {
        result.sort(compare);
    }

    return result;
}

function sortByStars(a, b) {
    // DESC
    return b.repository.stars - a.repository.stars;
}

function onSort($element, sort) {
    $element.addEventListener("click", function () {
        compare = sort;

        render(match(frameworks));
    });
}

const $moreStars = $("js-sort-by-stars");
onSort($moreStars, sortByStars);