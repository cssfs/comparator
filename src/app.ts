class Repository {
    constructor(public readonly name: string,
                public readonly url: string,
                public readonly starts: number,) {
    }
}

class Corporation {
    constructor(public readonly name: string,
                public readonly url: string,) {
    }
}

class Size {
    constructor(public readonly uncompressed: number,
                public readonly minified: number,
                public readonly gzipped: number,) {
    }
}

class Author {
    constructor(public readonly name: string) {

    }
}

class Framework {
    constructor(public readonly name: string,
                public readonly accessibility: boolean,
                public readonly siteUrl: string,
                public readonly repository: Repository,
                public readonly size: Size,
                public readonly documentationUrl: string,
                public readonly corporation: Corporation,
                public readonly requires: Array<string>,
                public readonly authors: Array<Author>,) {
    }
}

class FrameworkTableView {
    constructor(private readonly $element: HTMLElement) {

    }

    public render(list: Array<Framework>) {
        const rows = [];

        for (let i = 0; i < list.length; i++) {
            rows.push(
                this.renderFramework(list[i])
            );
        }

        this.$element.innerHTML = rows.join("");
    }

    private renderFramework(framework: Framework): string {
        return `
<tr>
    <td>${this.renderLink(framework.name, framework.siteUrl)}</td>    
    <td>${framework.accessibility ? "yes" : "no"}</td>                
    <td>${this.renderLink(framework.repository.name, framework.repository.url)}</td>            
    <td>${this.renderSize(framework.size)}</td>            
    <td>${this.renderLink("Docs", framework.documentationUrl)}</td>            
    <td>${this.renderCorporation(framework.corporation)}</td>            
    <td>${framework.repository.starts}</td>            
    <td>${framework.requires.join(", ")}</td>                    
    <td>${this.renderAuthors(framework.authors)}</td>            
</tr>
        `;
    }

    private renderSize(size: Size): string {
        return `uncompressed: ${size.uncompressed}, minified: ${size.minified}, gzipped: ${size.gzipped}`;
    }

    private renderCorporation(corporation: Corporation): string {
        return this.renderLink(corporation.name, corporation.url);
    }

    private renderAuthors(authors: Array<Author>): string {
        const names = [];

        for (let i = 0; i < authors.length; i++) {
            names.push(authors[i].name);
        }

        return names.join(", ");
    }

    private renderLink(name: string, url: string): string {
        return `<a href="${url}">${name}</a>`
    }
}

(function () {
    const $view = document.getElementById("js-framework-view");

    if ($view === null) {
        console.error(`undefined element by id "${"js-framework-view"}"`);

        return;
    }

    const frameworks = [
        new Framework(
            "Bootstrap",
            true,
            "https://getbootstrap.com/",
            new Repository(
                "GitHub",
                "https://github.com/twbs/bootstrap",
                130576
            ),
            new Size(
                187000,
                147000,
                20000,
            ),
            "https://getbootstrap.com/docs/4.1/getting-started/introduction/",
            new Corporation(
                "Twitter, Inc",
                "https://twitter.com/",
            ),
            [
                "jQuery’s slim build",
            ],
            [
                new Author("Mark Otto"),
                new Author("Jacob Thornton"),
            ],
        ),
        new Framework(
            "Foundation",
            true,
            "https://foundation.zurb.com/",
            new Repository(
                "GitHub",
                "https://github.com/zurb/foundation-sites",
                27971
            ),
            new Size(
                90000,
                64000,
                12000,
            ),
            "https://foundation.zurb.com/sites/docs/",
            new Corporation(
                "ZURB, Inc",
                "https://zurb.com/",
            ),
            [
                "jQuery",
            ],
            [
                new Author("Geoff Kimball"),
                new Author("Chris Oyler"),
            ],
        ),
        new Framework(
            "Materialize",
            false,
            "https://materializecss.com/",
            new Repository(
                "GitHub",
                "https://github.com/Dogfalo/materialize",
                36096
            ),
            new Size(
                114000,
                90000,
                18000,
            ),
            "https://materializecss.com/getting-started.html",
            new Corporation(
                "Materialize",
                "https://materializecss.com/",
            ),
            [
                "jQuery",
            ],
            [
                new Author("Alvin Wang"),
            ],
        ),
    ];

    const $frameworkListView = new FrameworkTableView($view);

    $frameworkListView.render(frameworks);
})();
