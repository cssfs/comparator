### CSS Frameworks compartor
 * Accessibility
 * Size, minified, gzipped
 * Docs
 * Corporation support
 * Stars
 * JavaScript required
 * Docs, books, courses
 * Authors

### Go to [cssfs.netlify.com](https://cssfs.netlify.com/)
### Go to [cssfs.dev](https://cssfs.dev/)
### Development
##### Start
```bash
sudo docker-compose up -d
```

##### Build
```bash
sudo docker exec css_framework_comparator webpack -c "--mode=production"
```
or
```bash
sudo docker exec -it css_framework_comparator bash
# in container run:
webpack --mode=production
```

##### Finish
 ```bash
 sudo docker-compose down
 ```
 
 ### Alternatives:
 * [A comparison of CSS library size](https://gist.github.com/primaryobjects/64a4e7e3351c646f51eee07949215ad4)